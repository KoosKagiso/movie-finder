import React, { Component } from 'react';
import SearchArea from './SearchArea';
import Nav from './Nav';
import MovieList from './MovieList';
import Pagination from './Pagination';
import MovieInfo from './MovieInfo';

class App extends Component {
  constructor() {
    super()
    this.state = {
      movies: [],
      searchTerm: '',
      totalResults: 0, // contains total movies i get back from the api request
      currentPage: 1,
      currentMovie: null
    }
    this.apiKey = 'dc011eaefb51cf52f0191e8c0ebd1420';
  }

  handleSubmit = (event) => {
    event.preventDefault();
    fetch(`https://api.themoviedb.org/3/search/movie?api_key=${this.apiKey}&query=${this.state.searchTerm}`)
      .then(data => data.json())
      .then(data => {
        console.log(data.results);
        this.setState({ movies: [...data.results], totalResults: data.total_results }); // use spread operator ... to spread out all the content from the resuls array into movie array 
      })
  }

  handleChange = (event) => {
    this.setState({ searchTerm: event.target.value })
  }

  nextPage = (pageNumber) => {
    fetch(`https://api.themoviedb.org/3/search/movie?api_key=${this.apiKey}&query=${this.state.searchTerm}&page=${pageNumber}`)
      .then(data => data.json())
      .then(data => {
        console.log(data.results);
        this.setState({ movies: [...data.results], currentPage: pageNumber });
      })
  }

  //set current movie to whatver i clicked on, and take movie id and get the movie object
  viewMovieInfo = (id) => {
    const filteredMovie = this.state.movies.filter(movie => movie.id == id);
    const newCurrentMovie = filteredMovie.length > 0 ? filteredMovie[0] : null;
    this.setState({ currentMovie: newCurrentMovie });
  }

  closeMovieInfo = () => {
    this.setState({ currentMovie: null });
  }

  render() {
    const numberPages = Math.floor(this.state.totalResults / 20);
    return (
      <div className="App">
        <Nav />

        {
          this.state.currentMovie == null ?
            <div>
              <SearchArea
                handleSubmit={this.handleSubmit}
                handleChange={this.handleChange}
              />
              <MovieList
                viewMovieInfo={this.viewMovieInfo}
                movies={this.state.movies} />

            </div> : <MovieInfo currentMovie={this.state.currentMovie} closeMovieInfo={this.closeMovieInfo} />
        }

        {this.state.totalResults > 20 ?
          <Pagination
            pages={numberPages}
            nextPage={this.nextPage}
            currentPage={this.state.currentPage} /> : ''}

      </div>
    )
  }
}

export default App;
