import React from 'react';

const Pagination = (props) => {
    const pageLinks = []; // this array hold all the li tags for the page number
    //created a for loop, take number of pages from the props, and iterate over the amount of pages,
    // and create the li tag with the corrrect number associate with pagination number

    for (let i = 1; i <= props.pages + 1; i++) {
        let active = props.currentPage == i ? 'active' : '';
        pageLinks.push(<li className={`waves-effect ${active}`} key={i} onClick={() => props.nextPage(i)}>
            <a href="#">
                {i}
            </a>
        </li>)
    }
    return (
        <div className="container">
            <div className="row">
                <ul className="pagination">
                    {props.currentPage > 1 ? <li className={`waves-effect`} onClick={() => props.nextPage(props.currentPage - 1)} >
                        <a href="#"><i className="fa fa-arrow-circle-left"></i></a>
                    </li> : ''}
                    {pageLinks}
                    {props.currentPage < props.pages + 1 ? <li className={`waves-effect`} onClick={() => props.nextPage(props.currentPage + 1)} >
                        <a href="#"><i className="fa fa-arrow-circle-right"></i></a>
                    </li> : ''}
                </ul>
            </div>
        </div>
    )
}

export default Pagination;