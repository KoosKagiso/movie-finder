import React from 'react';
import Movie from './Movie';

const MovieList = (props) => {
    return (
        <div className="container">
            <div className="row">
                <div className="col s12">
                    {
                        props.movies.map((movieItem, i) => {
                            return (
                                <Movie
                                    key={i}
                                    viewMovieInfo={props.viewMovieInfo}
                                    movieId={movieItem.id}
                                    image={movieItem.poster_path}
                                />
                            )
                        })
                    }
                </div>
            </div>

        </div>
    )

}

export default MovieList;