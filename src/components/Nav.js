import React from 'react';

const Nav = () => {
    return (
        <nav>
            <div className="nav-wrapper container">
                <a href="#" className="brand-logo">Kagiso's Movie Finder <i className="fa fa-film"></i></a>
            </div>
        </nav>
    )
}

export default Nav;